# Ohio AWS Console Extension

The Ohio AWS Console Extension displays an Ohio GIF when the region is set to Ohio on the AWS Console.

## Installation

To install the extension, follow these steps:

1. Clone the repository to your local machine:

```
git clone git@gitlab.com:jobcase-arosini/aws-ohio-warning-chrome-extension.git
```

2. Open Google Chrome and go to `chrome://extensions`.

3. Enable "Developer mode" by clicking the toggle switch in the top right corner.

4. Click "Load unpacked" and select the directory where you cloned the repository.

5. The extension should now be installed and ready to use.

## Usage

To use the extension, simply navigate to the AWS Console and set your region to Ohio. If the region is set to Ohio, an Ohio GIF will be displayed in a modal. The modal includes a close button that allows you to close the modal and return to the AWS Console.

## Contributing

Pull requests are not welcome since the code is perfect.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

