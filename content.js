const modal = document.createElement("div");
modal.innerHTML = `
  <div style="position: fixed; top: 0; bottom: 0; left: 0; right: 0; background-color: rgba(255,0,0,0.5); z-index: 999;">
    <div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
      <h1> WARNING -- YOU ARE IN </h1>
      <img src="https://media4.giphy.com/media/3o72Ff1iOlcRsldf8I/giphy-downsized.gif" style="max-width: 90%; max-height: 90%;">
      <button type="button" style="position: absolute; top: 0px; right: 0px; background-color: none; border: none; font-weight: bold; cursor: pointer;">x</button>
    </div>
  </div>
`;
document.body.appendChild(modal);
modal.querySelector('button').addEventListener('click', () => {
  modal.remove();
});